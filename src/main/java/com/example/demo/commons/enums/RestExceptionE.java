package com.example.demo.commons.enums;

import org.springframework.http.HttpStatus;

public enum RestExceptionE {

    PERSON_NOT_FOUND(1000, HttpStatus.NOT_FOUND, "person.not.exists"),
    ERROR_PERSON_ALREADY_EXISTS(1001, HttpStatus.BAD_REQUEST, "person.already.exists"),
    ;

    private final Integer id;
    private final HttpStatus httpStatus;
    private final String message;

    RestExceptionE(final Integer pId, final HttpStatus pHttpStatus, final String pMessage) {
        this.id = pId;
        this.httpStatus = pHttpStatus;
        this.message = pMessage;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @return the httpStatus
     */
    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }
}

package com.example.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.controllers.dto.PersonaDTO;

public interface PersonaRepository extends JpaRepository<PersonaDTO, Integer> {
	
    PersonaDTO findPersonaByTipoDocumentoAndNumeroDocumento(String tipoDocumento, String numeroDocumento);
}

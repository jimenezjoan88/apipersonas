package com.example.demo.config;

import com.example.demo.commons.enums.RestExceptionE;

public class RestException extends Exception {

	private static final long serialVersionUID = -7712184628430455888L;
	private final RestExceptionE info;

    public RestException(RestExceptionE pInfo) {
        super();
        this.info = pInfo;
    }

    public RestExceptionE getInfo() {
        return info;
    }
}
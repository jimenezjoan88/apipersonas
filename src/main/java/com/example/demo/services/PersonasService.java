package com.example.demo.services;

import java.util.List;

import com.example.demo.config.RestException;
import com.example.demo.controllers.dto.PersonaDTO;

public interface PersonasService {

    List<PersonaDTO> getPersonas();

    PersonaDTO getPersona(int id) throws RestException;

    void createPersona(PersonaDTO employeeDTO) throws RestException;

    void updatePersona(int id, PersonaDTO employeeDTO) throws RestException;

    void deletePersona(int id) throws RestException;
    
    void padrePersona(int idPersonaPadre, int idPersonaHijo) throws RestException;
    
    String relaciones(int idPersona1, int idPersona2) throws RestException;
}

package com.example.demo.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.commons.enums.RestExceptionE;
import com.example.demo.config.RestException;
import com.example.demo.controllers.dto.PersonaDTO;
import com.example.demo.repositories.PersonaRepository;

@Service
public class PersonaServiceImpl implements PersonasService {
	@Autowired
    private PersonaRepository personaRepository;

	@Override
	public List<PersonaDTO> getPersonas() {
		return personaRepository.findAll();
	}

	@Override
	public PersonaDTO getPersona(int id) throws RestException {
		Optional<PersonaDTO> persona = personaRepository.findById(id);
        if(!persona.isPresent()) {
            throw new RestException(RestExceptionE.ERROR_PERSON_ALREADY_EXISTS);
        }
		return persona.get();
	}

	@Transactional
	@Override
	public void createPersona(PersonaDTO newPersonaDTO) throws RestException {
		// Validar si existe
		PersonaDTO personaDB = personaRepository.findPersonaByTipoDocumentoAndNumeroDocumento(newPersonaDTO.getTipoDocumento(), newPersonaDTO.getNumeroDocumento());
		
		if (personaDB != null) {
            throw new RestException(RestExceptionE.ERROR_PERSON_ALREADY_EXISTS);
        }
		
		// Guardar persona
		newPersonaDTO = personaRepository.save(newPersonaDTO);
		
	}

	@Override
	public void updatePersona(int id, PersonaDTO employeeDTO) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deletePersona(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void padrePersona(int idPersonaPadre, int idPersonaHijo) throws RestException {
		PersonaDTO persona = getPersona(idPersonaHijo);

		if(idPersonaPadre > 0) {
			PersonaDTO padre = new PersonaDTO();
			padre.setId(idPersonaPadre);
			persona.setPersonaPadre(padre);
		}
		
		personaRepository.save(persona);
	}

	@Override
	public String relaciones(int idPersona1, int idPersona2) throws RestException {
		PersonaDTO persona1 = getPersona(idPersona1);
		PersonaDTO persona2 = getPersona(idPersona2);
		
		if(persona1.getPersonaPadre() == persona2.getPersonaPadre()) {
			return "HERMAN@";
		} else if(persona1.getPersonaPadre() != null && persona2.getPersonaPadre() != null &&
				persona1.getPersonaPadre().getPersonaPadre() == persona2.getPersonaPadre().getPersonaPadre()) {
			return "PRIM@S";
		} else if(persona1.getPersonaPadre() != null && persona2.getPersonaPadre() != null &&
				persona2.getPersonaPadre().getPersonaPadre() != null &&
				persona1.getPersonaPadre() == persona2.getPersonaPadre().getPersonaPadre()) {
			return "TI@";
		}
		return "Sin relación";
	}

}

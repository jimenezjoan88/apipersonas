package com.example.demo.controllers.dto;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;

@Entity
@Table(name = "persona")
public class PersonaDTO implements Serializable {
	private static final long serialVersionUID = -6682091046430566552L;

	@Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
	private int id;

	@NotNull(message = "El tipo de documento no puede estar vacío")
	@Pattern(regexp = "[a-zA-Z]+")
	@Column(name = "tipo_documento", length = 10)
    @Size(max = 10)
	private String tipoDocumento;

	@NotNull(message = "El numero de documento no puede estar vacío")
	@Column(name = "numero_documento", length = 15)
    @Size(max = 15)
	private String numeroDocumento;

	@NotNull(message = "El nombre no puede estar vacío")
	@Column(name = "nombre", length = 50)
    @Size(max = 50)
	private String nombre;

	@Min(value = 18, message = "El minimo de edad es 18 años")
	@Column(name = "edad")
	private Integer edad;

	@NotNull(message = "El pais no puede estar vacío")
	@ManyToOne
    @JoinColumn(name = "id_pais")
	private PaisDTO pais;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_padre")
	private PersonaDTO personaPadre;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	public PaisDTO getPais() {
		return pais;
	}

	public void setPais(PaisDTO pais) {
		this.pais = pais;
	}

	public PersonaDTO getPersonaPadre() {
		return personaPadre;
	}

	public void setPersonaPadre(PersonaDTO personaPadre) {
		this.personaPadre = personaPadre;
	}

	@Override
	public String toString() {
		return "PersonaDTO [id=" + id + ", tipoDocumento=" + tipoDocumento + ", numeroDocumento=" + numeroDocumento
				+ ", nombre=" + nombre + ", edad=" + edad + ", pais=" + pais + ", personaPadre=" + personaPadre + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((edad == null) ? 0 : edad.hashCode());
		result = prime * result + id;
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		result = prime * result + ((numeroDocumento == null) ? 0 : numeroDocumento.hashCode());
		result = prime * result + ((pais == null) ? 0 : pais.hashCode());
		result = prime * result + ((personaPadre == null) ? 0 : personaPadre.hashCode());
		result = prime * result + ((tipoDocumento == null) ? 0 : tipoDocumento.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PersonaDTO other = (PersonaDTO) obj;
		if (edad == null) {
			if (other.edad != null)
				return false;
		} else if (!edad.equals(other.edad))
			return false;
		if (id != other.id)
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		if (numeroDocumento == null) {
			if (other.numeroDocumento != null)
				return false;
		} else if (!numeroDocumento.equals(other.numeroDocumento))
			return false;
		if (pais == null) {
			if (other.pais != null)
				return false;
		} else if (!pais.equals(other.pais))
			return false;
		if (personaPadre == null) {
			if (other.personaPadre != null)
				return false;
		} else if (!personaPadre.equals(other.personaPadre))
			return false;
		if (tipoDocumento == null) {
			if (other.tipoDocumento != null)
				return false;
		} else if (!tipoDocumento.equals(other.tipoDocumento))
			return false;
		return true;
	}
	
	

}

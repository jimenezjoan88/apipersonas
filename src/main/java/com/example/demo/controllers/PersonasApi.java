package com.example.demo.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.demo.config.RestException;
import com.example.demo.controllers.dto.PersonaDTO;

import javax.validation.Valid;
import java.util.List;

@RequestMapping(value = "/personas")
public interface PersonasApi {

	@GetMapping
    ResponseEntity<List<PersonaDTO>> getPersonas() throws RestException;

    @GetMapping("/{id}")
    ResponseEntity<PersonaDTO> getPersona(@PathVariable int id) throws RestException;

    @PostMapping
    ResponseEntity<Void> createPersona(@Valid @RequestBody PersonaDTO employeeDTO) throws RestException;

    @PutMapping("/{id}")
    ResponseEntity<Void> updatePersona(@PathVariable int id, @Valid @RequestBody PersonaDTO employeeDTO) throws RestException;

    @DeleteMapping("/{id}")
    ResponseEntity<Void> deletePersona(@PathVariable int id) throws RestException;
    
    @PostMapping("/{idPersonaPadre}/padre/{idPersonaHijo}")
    ResponseEntity<Void> padrePersona(@PathVariable int idPersonaPadre, @PathVariable int idPersonaHijo) throws RestException;
    
    @GetMapping("/relaciones/{idPersona1}/{idPersona2}")
    ResponseEntity<String> relaciones(@PathVariable int idPersona1, @PathVariable int idPersona2) throws RestException;
}

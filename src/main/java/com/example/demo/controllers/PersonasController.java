package com.example.demo.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.config.RestException;
import com.example.demo.controllers.dto.PersonaDTO;
import com.example.demo.services.PersonasService;

@RestController
public class PersonasController implements PersonasApi {
	@Autowired
    private PersonasService personasService;

	@Override
	public ResponseEntity<List<PersonaDTO>> getPersonas() throws RestException {
        return ResponseEntity.ok().body(personasService.getPersonas());
	}

	@Override
	public ResponseEntity<PersonaDTO> getPersona(int id) throws RestException {
		PersonaDTO personaDto = personasService.getPersona(id);

        // Validar si existe
        if (personaDto == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().body(personaDto);
	}

	@Override
	public ResponseEntity<Void> createPersona(@Valid PersonaDTO personaDTO) throws RestException {
		personasService.createPersona(personaDTO);

        return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@Override
	public ResponseEntity<Void> updatePersona(int id, @Valid PersonaDTO personaDTO) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Void> deletePersona(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Void> padrePersona(int idPersonaPadre, int idPersonaHijo) throws RestException {
		personasService.padrePersona(idPersonaPadre, idPersonaHijo);

        return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@Override
	public ResponseEntity<String> relaciones(int idPersona1, int idPersona2) throws RestException {
		return ResponseEntity.ok().body(personasService.relaciones(idPersona1, idPersona2));
	}

}

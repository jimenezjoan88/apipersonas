## Configurar en entorno local (Eclipse)

Importar proyecto en Eclipse

1. Desde consola posicionarse dentro de la carpeta del proyecto
2. Ejecutar el comando mvn clean eclipse:eclipse
3. Importar el proyecto, como proyecto existente.
4. Ejecutar el script **personas.sql** en una base de datos MySQL.

---

## Configurar en entorno local (IntelliJ)

Importar proyecto en Eclipse

1. Importar el proyecto.
2. Ejecutar el script **personas.sql** en una base de datos MySQL.

---

## Configurar datasourcce

Abrir el archivo application.yml y configurar las siguientes propiedades

1. **url**: La url de la base de datos, por defecto es jdbc:mysql://localhost:3306/api_reba
2. **username**: El usuario de la base de datos, por defecto es root
3. **password**: El usuario de la base de datos, por defecto esta vacio

---

## Levantar el servidor

Comando para levantar el servidor integrado

1. Desde consola, posicionarse en la carpeta del proyecto
2. Ejecutar el comando **mvn spring-boot:run**

---

## Probar la API

Este paso es opcional si se quiere probar la API desde Postman. Algunos paises ya estan cargados.

1. Importar en Postman la coleccion API Personas REBA.postman_collection.json
2. Crear personas
3. Crear padres e hijos